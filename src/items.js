
import e from 'express'
import express from 'express'
const router = express.Router()
const items = [{id: 1, task: "buy groceries"}, {id:2, task: "clean room"}]

router.get('/', (req, res) => {
    res.send(items)
})

router.post('/', (req, res) => {
    items.push(req.body)
    if(req.body.id == null || req.body.task == null ){
        return res.status(400).send(items)
    }
    else{
        return res.status(201).send(req.body)
    }
})

router.get('/:id', (req, res) => {
    items.forEach (element => {
        if(element.id == req.params.id){
            res.send(element)
        }
    })
})

router.put('/:id', (req, res) => {
    let i
    if(req.body.id != req.params.id){
        return res.status(400)
    } else{
        for(let i=0; i < items.length; i++){
            if(items[i].id == req.body.id){
                items[i].task = req.body.task
                return res.status(201).send(items[i])
            }    
            }
            if(i == items.length){
                return res.status(404)   
        }
    }

})

router.delete('/:id', (req, res) => {
    items.forEach (element => {
        if(element.id == req.params.id){
            const index = items.indexOf(element)
            items.splice(index, 1)
            return res.status(201).send(items)

        }
    })

})

export default router
