import itemRoutes from './src/items.js'
import express from 'express'
const app = express()
const port = 3002

// allows us to parse json 
app.use(express.json())
app.use('/items', itemRoutes)
app.get('/', (req, res) => res.send('Hello world'))

app.listen(port, () => console.log(`API server ready on http://localhost:${port}`))
